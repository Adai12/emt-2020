package mk.ukim.finki.emt.fss.xport.listeners;

import mk.ukim.finki.emt.fss.model.Student;
import mk.ukim.finki.emt.fss.model.events.StudentCreatedEvent;
import mk.ukim.finki.emt.fss.service.StudentService;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StudentEventHandlers {

    private final StudentService studentService;

    public StudentEventHandlers(StudentService studentService) {
        this.studentService = studentService;
    }

    @EventListener
    public void onStudentCreated(StudentCreatedEvent event) {
        System.out.println(((Student)event.getSource()).getName());
        this.studentService.refreshMV();
    }
}
