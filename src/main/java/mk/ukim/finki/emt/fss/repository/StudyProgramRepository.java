package mk.ukim.finki.emt.fss.repository;

import mk.ukim.finki.emt.fss.model.StudyProgram;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudyProgramRepository extends JpaRepository<StudyProgram,Long> {
}
