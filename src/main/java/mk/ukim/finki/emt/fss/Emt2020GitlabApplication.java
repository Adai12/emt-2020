package mk.ukim.finki.emt.fss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Emt2020GitlabApplication {

    public static void main(String[] args) {
        SpringApplication.run(Emt2020GitlabApplication.class, args);
    }

}
