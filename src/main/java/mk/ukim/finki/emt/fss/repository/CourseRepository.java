package mk.ukim.finki.emt.fss.repository;

import mk.ukim.finki.emt.fss.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course,Long> {
}
