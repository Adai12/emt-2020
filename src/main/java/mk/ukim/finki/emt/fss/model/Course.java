package mk.ukim.finki.emt.fss.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name="course")
public class Course {

    @Id
    private Long id;
    private String name;

    @ManyToMany(mappedBy = "courses")
    private List<Student> students;

    @ManyToOne
    private StudyProgram studyProgram;

}
